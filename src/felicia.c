/* FELICIA - Finite Element (Light C Implementation) Analysis
 * v0 - Basic test
 *
 * Mark D. Hare - 2017
 * GNU GPLv3
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lapacke.h>
#include <argp.h>

#include "config/felicia_config.h"

#include "lapacke_test.h"

#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)
#define VERSION_STRING "v" STRINGIZE(FELICIA_VERSION_MAJOR) \
	"." STRINGIZE(FELICIA_VERSION_MINOR)

const char *argp_program_version = "FELICIA " VERSION_STRING;
static char doc[] = "FELICIA - Finite Element (LIght C Implementation) Analysis.\nCopyright (c) Mark Hare 2017.";
static struct argp argp = {0, 0, 0, doc};


int
main(int argc, char **argv)
{
	/* fprintf(stderr, "FELICIA %s - (c) 2017 Mark Hare\n", VERSION_STRING); */

	argp_parse(&argp, argc, argv, 0, 0, 0);

	lapacke_test();

	return 0;
}

