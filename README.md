<center>![FELICIA Logo](res/logo_small.png) </center>
# FE(LICI)A - Finite Element (LIght C Implementation) Analysis
Created by Mark D. Hare  
markhare .at. buffalo .dot. edu

This software is licensed under the GNU GPLv3. See 'LICENSE' file for details.

## Dependencies:
- CMake
- Compatible C compiler and standard library
- LAPACKE
- GNU Make or other CMake supported build system
- GNU Argp

## To-Do:

- [ ] Write description
- [ ] Create directory structure and Makefile
- [ ] Implement command line arg processor - argp? getopt?
- [ ] Create data structures
- [ ] Implement code to process input - plain text or ABAQUS
- [ ] Solver Code - LAPACK?
- [ ] Post-processing
